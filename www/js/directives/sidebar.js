angular.module('ionic')
.controller('sidebarCtrl', function(loginService, $scope, $state, $ionicSideMenuDelegate, $stateParams) {
    var LS = JSON.parse(window.localStorage.getItem('user'));

    $scope.getItems = function () {
      LS = JSON.parse(window.localStorage.getItem('user'));
      if(!_.isNull(LS)) {
        var username = LS.user;
        var pass = LS.password;
        loginService.userLogin(username, pass).then(function(response){
            $scope.categories = response;
        }).catch(function(error){
           $state.go('login');
        })
      }
    }
    $scope.toggleLeftSideMenu = function() {
       $ionicSideMenuDelegate.toggleLeft();
    };
    $scope.goToCamera = function(camera) {
        $ionicSideMenuDelegate.toggleLeft();
        $state.go('video.recordings', {cam: camera.TITLE, day: 1});
    }
    $scope.signout = function() {
        window.localStorage.clear();
        $state.go('login');
    }
    $scope.isActive = function (category) {
        if (category.TITLE === $stateParams.cam) return true;
        else return false;
    }
    $scope.getItems();
})
