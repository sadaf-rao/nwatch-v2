angular.module('ionic')
.service('playerService', function ($http) {
  /**
    * Makes the settings for jwplayer
    * {Params} {stram Object}
    * returns {playersettings} {object}
    **/

  getOptions = function(stream) {
    const file = stream.file ? stream.file : stream.URL;
    return {
      playlist: [{
        file: file
      }],
      primary: "HTML5",
      abouttext: "iVOD",
      autostart: false,
      aboutlink: "http://nayatel.com/value-added-services/ivod",
      aspectratio: "16:9",
      width:"100%",
      logo: {
        file: '',
        link: 'http://vod.nayatel.com'
      }
    }
  }

  this.setupPlayer = function(item) {
    return {
      options: getOptions(item),
      time: item.title
    }
  }

  this.setupPlayerLive = function(item) {
    return {
      options: getOptions(item),
      camera: item.TITLE
    }
  }
})
//the service which will log the user in
