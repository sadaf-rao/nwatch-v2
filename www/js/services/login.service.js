angular.module('ionic')
.service('loginService', function ($http) {
    this.LS = window.localStorage;
    this.user = '';
    this.pass = '';
    this.getAllStreams = "http://smart.nayatel.com/NWatchApi/getAllStreams.php";
    this.getAllRecs = "http://smart.nayatel.com/NWatchApi/getAllRecs.php";
    this.getAllLive = "http://smart.nayatel.com/NWatchApi/getTitleWiseStream.php";
    this.camList = [];
    // Log the user over here;
    this.userLogin = function(username, pass) {
      //+'&streamday=1&streamname=HRENTRANCE'
      var that = this;
      that.user = username;
      that.pass = pass;
      const body = 'userid='+username+'&password='+pass;
      var config = {
          headers : {
              'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          },
          cache: true
      }
      return $http.post(this.getAllStreams, body, config).then(function(response){
          if(response.data[0] && response.data[0].Responce === "Invalid Credentials") {
            return Promise.reject(response.data[0].Responce);
          } else {
            // store the user information over here;
            that.LS.setItem('user', JSON.stringify({user: username, password: pass}));
            that.camList = response.data;
            return response.data;
          }
      });
    }
    // Getting the camera feeds
    this.getRecs = function(cam, day) {

      var that = this;
      var username = that.user ? that.user : JSON.parse(that.LS.getItem('user')).user;
      var pass = that.pass ? that.pass : JSON.parse(that.LS.getItem('user')).password;
      var config = {
          headers : {
              'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          },
          cache: true
      }
      const body = 'userid='+username+'&password='+pass+'&streamday='+day+'&streamname=' + cam;
      return $http.post(that.getAllRecs, body, config);
    }

    this.getLiveFeed = function(cam) {
      var that = this;
      var username = that.user ? that.user : JSON.parse(that.LS.getItem('user')).user;
      var pass = that.pass ? that.pass : JSON.parse(that.LS.getItem('user')).password;
      var config = {
          headers : {
              'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          },
          cache: true
      }
      const body = 'userid='+username+'&password='+pass;
      return $http.post(that.getAllStreams, body, config);
    }
})
//the service which will log the user in
