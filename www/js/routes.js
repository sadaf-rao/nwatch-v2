angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
  .state('login', {
    url: '/login',
    abstract: false,
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl',
    controllerAs: 'vm'
  })
  .state('video', {
    url: '/video',
    abstract: true,
    templateUrl: 'templates/video-dash.html',
  })
  .state('video.recordings', {
    url: '/video/recording/:cam/:day',
    views: {
      'recordings': {
        templateUrl: 'templates/recordings.html',
        controller: 'recordingsCtrl',
        controllerAs: 'vm'
      },
      params: {
          cam: null,
          day: null
      }
    },
    resolve: {
      recordingFeed: ['loginService', '$stateParams',function(loginService, $stateParams) {
        return loginService.getRecs($stateParams.cam, $stateParams.day)
      }],
      camList: ['loginService','$state',function(loginService, $state) {
        var LS = JSON.parse(window.localStorage.getItem('user'));
        var username = LS.user;
        var pass = LS.password;
        return loginService.userLogin(username, pass).then(function(response){
          return response;
        }).catch(function(error){
           $state.go('login');
        })
      }]
    }
  })
  .state('video.live', {
    url: '/video/live',
    views: {
      'live': {
        templateUrl: 'templates/live.html',
        controller: 'liveCtrl',
        controllerAs: 'vm'
      }
    },
    resolve: {
      camList: function(loginService, $state) {
        var LS = JSON.parse(window.localStorage.getItem('user'));
        var username = LS.user;
        var pass = LS.password;
        return loginService.userLogin(username, pass).then(function(response){
          return response;
        }).catch(function(error){
           $state.go('login');
        })
      }
    }
  })
$urlRouterProvider.otherwise('/login')

});
