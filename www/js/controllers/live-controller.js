angular.module('ionic')
.controller('liveCtrl', function ($scope, $stateParams, loginService, camList, playerService) {

    /**
      * To get the live streams per camera
      * Requires loginService
      **/
      const cards = [];
      _.mapValues(camList, function(cam){
          const data = playerService.setupPlayerLive(cam);
          cards.push(data);
      });
      this.liveCards = cards;

      /**
        * to open the tab
        * opening the tab displayes the feed
        **/
        
      this.makeStream = function(stream, index) {
          stream.showStream === false ? stream.showStream = true :
            stream.showStream === true ? stream.showStream = false :
            stream.showStream = true;
      }
});
