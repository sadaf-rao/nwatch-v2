angular.module('app.controllers', [])
.controller('loginCtrl', function (loginService, $state){
    // Adding the user over here
    this.loginService = loginService;
    this.user = {

    }

    this.submit = function() {
      if(this.user.username.length>0 && this.user.password.length>0) {
        this.loginService.userLogin(this.user.username, this.user.password).then(function(response){
          const totalCams = Object.keys(response).length;
          $state.go('video.recordings', {cam: response[1].TITLE, day: 1});
          // if the response is valid aka success, continue to nect state
        }).catch(function(error){
        });
      } else {
        return 'error';
      }
    }
});
