angular.module('ionic')
  .controller('recordingsCtrl',
   function($scope, $stateParams, recordingFeed, camList, loginService, $q, playerService) {

    /**
      * Function that returns the tab streams for a specific camera
      * and for a specific day
      **/
    this.date = moment().format('l');
    this.day = 1;
    this.camera = $stateParams.cam;
    getTabStreams = function(title, day) {
        const data = [];
        var promise = loginService.getRecs(title,day).then(function(response){
            response.data.map(function(item){
              if(day === 1) {
                  const start = _.split(_.split(item.file, '.stream_')[1], '.')[0];
                  if (moment(start, 'YYYY-MM-DD_HH').isBefore(moment())) {
                    const items =  playerService.setupPlayer(item);
                    data.push(items);
                  }
              } else {
                  const items =  playerService.setupPlayer(item);
                  data.push(items);
              }
            });
            return data
        });
        return data;
    }
    /**
      * Previous and next date functions
      * Gets the streams according to the date
      **/
    this.getData = function (cam, day) {
      const streams = getTabStreams(cam, day)
      const stream = {
          title: cam,
          streams: getTabStreams(cam, day)
      }
      this.streamTabs = stream;
    }
    this.changeDate = function(type) {

        switch (type) {
          case 'next':
            if (this.day - 1 >= 1) {
                this.day -= 1;
                this.date = moment(this.date).add(1, 'days').format('l');
                this.getData($stateParams.cam, this.day);
            }
            break;
          case 'prev':
            if (parseInt(this.day) + 1 <=3 ) {
                this.day += 1;
                this.date = moment(this.date).subtract(1, 'days').format('l');
                this.getData($stateParams.cam, this.day);
            }
            break;
          default:
        }
    }
    /**
      * For each available camera make a new tab
      * First, fetch the list of cameras
      * Get it from resolve function
      **/
    const streamInitial = {
            title: camList['1'].TITLE,
            streams: getTabStreams(camList['1'].TITLE, 1)
    }
    /*const streamTabs = _.mapValues(camList, function(cam){
        return {
          title: cam.TITLE,
          tabStreams: getTabStreams(cam.TITLE, 1)
        }
    });*/
    this.streamTabs = streamInitial;
    this.streams = recordingFeed.data.map(function(item) {
      return {
        options: getOptions(item),
        time: item.title
      }
    });
    this.options = {
      loop: false,
      effect: 'fade',
      speed: 500,
    }

    this.streamDays = recordingFeed.data;
    this.camera = $stateParams.cam;
    this.date = this.day === 1 ? moment().format('l') :
      this.day === 2 ? moment().subtract('1', 'days').format('l') :
      this.day === 3 ? moment().subtract('2', 'days').format('l') : moment().format('l');
    // setting the class


    this.makeStream = function(stream, index) {
      stream.showStream === false ? stream.showStream = true :
        stream.showStream === true ? stream.showStream = false :
        stream.showStream = true;
    }
  });
